module github.com/phbarton/Telemetry-Go

go 1.15

require (
	github.com/gookit/color v1.3.1
	github.com/microsoft/ApplicationInsights-Go v0.4.3
)
